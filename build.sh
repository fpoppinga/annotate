#!/bin/sh

set -e
set -x

BASEDIR=$(realpath $(dirname $0))

npm_module() {
   MODULE=$1
   SCRIPTS=$2
   DIR=$(pwd)
   cd $BASEDIR/$MODULE
   npm $SCRIPTS
   cd $DIR
}

npm_module client "run build"
npm_module server "run clean"
npm_module server "run build"

