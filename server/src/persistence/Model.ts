import { Label } from '../api/model/Image';
import { Migratable } from './JsonService';
import { LabelsV2 } from '../api/model/LabelsV2';

export interface ConfigV1 {
  labels: string[];
}

export interface ConfigV2 extends Migratable {
  labels: LabelsV2;
}

export interface LabelData {
  data: {
    [key: string]: Label;
  };
}
