import * as e from 'express';
import { LabelService } from '../business/LabelService';

export class Statics {
  private labelService = LabelService.instance;

  statics = e.static('public');
  images = e.static(this.labelService.getDataDir());
}
