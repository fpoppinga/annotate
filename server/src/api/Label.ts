import * as express from 'express';
import { ConfigService } from '../business/ConfigService';

export class LabelV1 {
  protected labelService = ConfigService.instance;

  public get = (req: express.Request, res: express.Response) => {
    res.json(this.labelService.getBoxLabels());
  }
}

export class LabelV2 {
  protected labelService = ConfigService.instance;

  public get = (req: express.Request, res: express.Response) => {
    res.json(this.labelService.getLabels().labels);
  }
}
