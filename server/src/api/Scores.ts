import * as e from 'express';
import { ScoreService } from '../business/ScoreService';

export class Scores {
  private labelService = ScoreService.instance;

  public get = (req: e.Request, res: e.Response) => {
    res.json(this.labelService.getScores());
  }
}
