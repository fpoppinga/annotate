import * as express from 'express';
import { WithAuth } from './Auth';
import { Label, PostImage } from './model/Image';
import { LabelService } from '../business/LabelService';
import { ScoreService } from '../business/ScoreService';

export class Image {
  private labelService = LabelService.instance;
  private scoreService = ScoreService.instance;

  public postV1 = (req: express.Request, res: express.Response) => {
    const data: PostImage = req.body;
    const label: Label = {
      ...data.label,
      boxes: data.label.boxes.map(b => ({
        ...b,
        start: {
          x: b.start.x / 640,
          y: b.start.y / 480
        },
        size: {
          x: b.size.x / 640,
          y: b.size.y / 480
        }
      }))
    };
    this.update(data.authToken, label, res);
  }

  public postV2 = (req: express.Request, res: express.Response) => {
    const data: PostImage = req.body;
    this.update(data.authToken, data.label, res);
  }

  private update(authToken: string | undefined, label: Label, res: express.Response) {
    if (authToken) {
      WithAuth(authToken, d => {
        const value = label.boxes.length + 1;
        this.scoreService.addScore(d.login, value);
      });
    }
    if (this.labelService.set(label)) {
      res.sendStatus(200);
      return;
    }
    res.sendStatus(400);
  }

  public getV1 = (req: express.Request, res: express.Response) => {
    this.labelService.getImage()
      .then(l => ({
        ...l,
        boxes: l.boxes.map(b => ({
          ...b,
          start: {
            x: b.start.x * 640,
            y: b.start.y * 480
          },
          size: {
            x: b.size.x * 640,
            y: b.size.y * 480
          }
        }))
      }))
      .then(l => {
        res.json(l);
      })
      .catch(() => {
        res.sendStatus(404);
      });
  }

  public getV2 = (req: express.Request, res: express.Response) => {
    this.labelService.getImage()
      .then(l => {
        res.json(l);
      })
      .catch(() => {
        res.sendStatus(404);
      });
  }
}
