import * as lockfile from 'lockfile';

export default class Lock {
  private file: string;

  constructor(file: string) {
    this.file = file;
  }

  public lock(cb: () => void) {
    lockfile.lock(
      this.file,
      () => {
        try {
          cb();
        } finally {
          lockfile.unlock(this.file, console.error);
        }
      }
    );
  }
}
