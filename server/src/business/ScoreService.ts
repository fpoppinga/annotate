import { Scores } from '../api/model/Scores';
import { getDataDir } from './Util';
import * as path from 'path';
import { JsonService } from '../persistence/JsonService';

export class ScoreService {
  public static instance = new ScoreService();
  private scores: JsonService<Scores, Scores>;

  getScores(): Scores {
    return this.scores.get();
  }

  addScore(user: string, value: number): void {
    this.scores.update(c => {
      if (c[user]) {
        c[user] += value;
      } else {
        c[user] = value;
      }
      return c;
    });
  }

  private constructor() {
    this.scores = new JsonService<Scores, Scores>(path.join(getDataDir(), 'scores.json'), {});
  }
}
