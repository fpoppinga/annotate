import { Label } from '../api/model/Image';
import { getDataDir } from './Util';
import { AnnotationService } from '../persistence/AnnotationService';

export class LabelService {
  public static instance = new LabelService();
  private dataDir: string;
  private label: AnnotationService<Label>;

  getImage(): Promise<Label> {
    return this.label.getRandom();
  }

  set(label: Label): Promise<{}> {
    return this.label.set(label.url, label);
  }

  getDataDir(): string {
    return this.dataDir;
  }

  private constructor() {
    this.dataDir = getDataDir();
    console.log(`Using ${this.dataDir}`);

    this.label = new AnnotationService<Label>(key => ({url: key, boxes: []}));
  }
}

/*
function find(filePath: string, suffix: string): string[] {
  let result: string[] = [];
  for (let file of fs.readdirSync(filePath)) {
    let fullPath = path.join(filePath, file);
    const stats = fs.lstatSync(fullPath);
    if (stats.isDirectory()) {
      result = result.concat(find(fullPath, suffix));
    }
    if (stats.isFile() && file.indexOf(suffix, -suffix.length) > 0) {
      result = result.concat(fullPath);
    }
  }
  return result;
}
*/
