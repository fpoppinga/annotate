import * as fs from 'fs';

export function getDataDir(): string {
  const dataDir = process.env.DATA_DIR as string;
  if (dataDir && fs.existsSync(dataDir)) {
    return dataDir;
  }
  return fs.mkdtempSync('annotate');
}
