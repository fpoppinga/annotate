import * as path from 'path';
import { JsonService, Migratable, Migration } from '../persistence/JsonService';
import { getDataDir } from './Util';
import { ConfigV1, ConfigV2 } from '../persistence/Model';

const initialConfig: ConfigV1 = {
  labels: ['ball', 'robot']
};

const configMigrations: Migration<Migratable>[] = [
  {
    key: 'v2',
    apply: (v1: ConfigV1): ConfigV2 => ({
      migrations: [],
      labels: {
        images: ['game', 'other'],
        boxes: v1.labels
      }
    })
  }
];

export class ConfigService {
  public static instance = new ConfigService();
  private config: JsonService<ConfigV1, ConfigV2>;

  constructor() {
    this.config = new JsonService<ConfigV1, ConfigV2>(
      path.join(getDataDir(), 'annotate.json'),
      initialConfig,
      configMigrations
    );
  }

  getBoxLabels(): string[] {
    return this.config.get().labels.boxes;
  }

  getLabels(): ConfigV2 {
    return this.config.get();
  }
}
