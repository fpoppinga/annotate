import { LabelData } from './persistence/Model';
import * as fs from 'fs';
import axios from 'axios'
import { PostImage } from './api/model/Image';

const dataJson = 'data/label.json';
const server = 'http://localhost:3000';

const data: LabelData = JSON.parse(fs.readFileSync(dataJson, 'UTF-8'));

const keys = Object.keys(data.data);
for (let i = 0; i < keys.length; i++) {
  const key = keys[i];
  const value = data.data[key];
  const post: PostImage = {
    label: value
  };

  axios
    .post(`${server}/v1/image`, post)
    .then(r => console.log(`Updated ${key}`))
    .catch(console.error);
}
