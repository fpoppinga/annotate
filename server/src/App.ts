import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Auth } from './api/Auth';
import { Image } from './api/Image';
import { LabelV1, LabelV2 } from './api/Label';
import { Scores } from './api/Scores';
import { Statics } from './api/Statics';

class App {
  public express: express.Application;
  private image = new Image();
  private labelV1 = new LabelV1();
  private labelV2 = new LabelV2();
  private score = new Scores();
  private statics = new Statics();

  constructor() {
    this.express = express();
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({extended: true}));
    this.express.use(express.json());
    this.express.use(express.urlencoded());
    this.mountRoutes();
  }

  private mountRoutes(): void {
    this.express.post('/v1/auth', Auth);
    this.express.post('/v1/image', this.image.postV1);
    this.express.get('/v1/image', this.image.getV1);
    this.express.get('/v1/labels', this.labelV1.get);
    this.express.get('/v1/scores', this.score.get);

    this.express.get('/v2/labels', this.labelV2.get);
    this.express.get('/v2/image', this.image.getV2);
    this.express.post('/v2/image', this.image.postV2);

    this.express.use(this.statics.statics);
    this.express.use(this.statics.images);
  }
}

export default new App().express;
