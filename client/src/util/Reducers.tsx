import { Dispatch } from 'react-redux';

export interface VoidAction<P> {
  type: string;
  dispatch: Dispatch<{}>;
  props: P;
}

export interface Action<T, P> extends VoidAction<P> {
  value: T;
}

export class ActionHandler<T, P, S> {
  private static typeCounter = 0;
  private type: string;
  private reducer: (s: S, v: Action<T, P>) => S;

  constructor(f: (s: S, a: Action<T, P>) => S) {
    this.type = `s-${ActionHandler.typeCounter++}`;
    this.reducer = f;
  }

  public apply(s: S, a: Action<{}, {}>): S {
    if (a.type === this.type) {
      return this.reducer(s, (a as Action<T, P>));
    }
    return s;
  }

  public create(dispatch: Dispatch<{}>, props: P): (v: T) => void {
    return (value: T) => dispatch(
      {
        type: this.type,
        value: value,
        dispatch: dispatch,
        props: props
      });
  }
}

export class VoidActionHandler<S, P> {
  private static typeCounter = 0;
  private type: string;
  private reducer: (s: S, v: VoidAction<P>) => S;

  constructor(f: (s: S, a: VoidAction<P>) => S) {
    this.type = `v-${VoidActionHandler.typeCounter++}`;
    this.reducer = f;
  }

  public apply(s: S, a: VoidAction<{}>): S {
    if (a.type === this.type) {
      return this.reducer(s, a as VoidAction<P>);
    }
    return s;
  }

  public create(dispatch: Dispatch<{}>, props: P): () => void {
    return () => dispatch(
      {
        type: this.type,
        dispatch: dispatch,
        props: props
      });
  }
}

export class ReducerFactory<S, P> {
  private voidActions: VoidActionHandler<S, P>[];
  private actions: ActionHandler<{}, P, S>[];

  public constructor() {
    this.voidActions = [];
    this.actions = [];
  }

  public createAction<V>(f: (s: S, a: Action<V, P>) => S) {
    const actionHandler = new ActionHandler<V, P, S>(f);
    this.actions.push(actionHandler);
    return actionHandler;
  }

  public createVoidAction(f: (s: S, a: VoidAction<P>) => S) {
    const actionHandler = new VoidActionHandler(f);
    this.voidActions.push(actionHandler);
    return actionHandler;
  }

  public build(initialState: S) {
    return (state: S = initialState, action: Action<{}, {}>): S => {
      const step1 = this.actions.reduce((s, a) => a.apply(s, action), state);
      return this.voidActions.reduce((s, a) => a.apply(s, action), step1);
    };
  }
}
