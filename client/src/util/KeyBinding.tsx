import * as React from 'react';
import { isUndefined } from 'util';

export class KeyBinding extends React.Component {
  props: {
    code: string,
    ctrl?: boolean,
    shift?: boolean,
    onKey: (e: KeyboardEvent) => void,
    keepDefault?: boolean
  };

  componentDidMount() {
    document.addEventListener('keydown', e => this.handleKey(e));
  }

  componentWillUnMount() {
    document.removeEventListener('keydown', e => this.handleKey(e as KeyboardEvent));
  }

  handleKey(e: KeyboardEvent) {
    if (e.code !== this.props.code) {
      return;
    }
    if (checkMetaKey(e.ctrlKey, this.props.ctrl)) {
      return;
    }
    if (checkMetaKey(e.shiftKey, this.props.shift)) {
      return;
    }
    if (!this.props.keepDefault) {
      e.preventDefault();
    }
    this.props.onKey(e);
  }

  render() {
    return <div/>;
  }
}

function checkMetaKey(meta: boolean, predicate?: boolean): boolean {
  return !isUndefined(predicate) && predicate !== meta;
}
