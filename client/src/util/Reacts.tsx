import { Point, Points } from '../api/Point';

export function renderIf<T>(predicate: boolean, then: () => T, fallback: () => T | null = () => null): T | null {
  if (predicate) {
    return then();
  }
  return fallback();
}

export const fromMouseClick = (mouseEvent: React.MouseEvent<{}>): Point => {
  return {
    x: mouseEvent.clientX,
    y: mouseEvent.clientY
  };
};

export const fromTouchClick = (touchEvent: React.TouchEvent<{}>): Point => {
  return {
    x: touchEvent.targetTouches[0].clientX,
    y: touchEvent.targetTouches[0].clientY
  };
};

export const fromMouseEvent = (clickPoint: Point, mouseEvent: React.MouseEvent<{}> | React.TouchEvent<{}>): Point => {
  const element = (mouseEvent.currentTarget as Element);
  const image = element.getElementsByClassName('image')[0].getBoundingClientRect();
  const imageOrigin: Point = {
    x: image.left,
    y: image.top
  };
  const imageSize: Point = {
    x: image.width,
    y: image.height
  };
  const capped: Point = Points.cap(
    Points.minus(clickPoint, imageOrigin),
    {x: 0, y: 0},
    imageSize
  );
  const relative: Point = Points.reduce(
    capped,
    imageSize,
    (c, i) => c / i
  );
  return relative;
};
