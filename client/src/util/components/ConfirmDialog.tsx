import { Button, Modal } from 'react-bootstrap';
import * as React from 'react';

export const ConfirmDialog = (props: {
  title?: string,
  body: string,
  yes?: string,
  no?: string,
  onYes?: () => void,
  onNo?: () => void
}) => (
  <div className="static-modal">
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>
          {props.title ? props.title : ""}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.body}
      </Modal.Body>
      <Modal.Footer>
        <Button
          onClick={e => {
            if (props.onNo) {
              props.onNo();
            }
          }}
        >
          {props.no ? props.no : "Cancel"}
        </Button>
        <Button
          bsStyle="primary"
          onClick={e => {
            if (props.onYes) {
              props.onYes();
            }
          }}
        >
          {props.yes ? props.yes : "Ok"}
        </Button>
      </Modal.Footer>

    </Modal.Dialog>
  </div>
);
