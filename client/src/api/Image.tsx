import { Point } from "./Point";

export interface PostImage {
  label: Label
  authToken?: string
}

export interface GetAuth {
  code: string
}

export interface Label {
  url: string,
  label?: string,
  boxes: Box[]
}

export interface Box {
  label: string,
  start: Point,
  size: Point
}


