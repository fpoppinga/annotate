export interface GenericPoint<T> {
  x: T;
  y: T;
}

export interface Point extends GenericPoint<number> {
}

export module Points {
  export function minus(p1: Point, p2: Point): Point {
    return reduce(p1, p2, (a, b) => a - b);
  }

  export function plus(p1: Point, p2: Point): Point {
    return reduce(p1, p2, (a, b) => a + b);
  }

  export function cap(p: Point, min: Point, max: Point) {
    return reduce(reduce(p, max, Math.min), min, Math.max);
  }

  export function times(p: Point, f: number): Point {
    return apply(p, v => v * f);
  }

  export function divide(p: Point, f: number): Point {
    return apply(p, v => v / f);
  }

  export function squareAbs(p1: Point): number {
    return (p1.x * p1.x) + (p1.y * p1.y);
  }

  export function apply<S, T>(p: GenericPoint<S>, f: (v: S) => T): GenericPoint<T> {
    return {
      x: f(p.x),
      y: f(p.y)
    };
  }

  export function reduce<T>(p1: GenericPoint<T>, p2: GenericPoint<T>, f: (v1: T, v2: T) => T): GenericPoint<T> {
    return {
      x: f(p1.x, p2.x),
      y: f(p1.y, p2.y)
    };
  }
}
