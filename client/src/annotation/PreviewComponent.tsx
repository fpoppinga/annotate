import * as React from 'react';
import { Point } from '../api/Point';

export const Preview = (props: {
  imageUrl: string,
  stateStart: Point,
  stateSize: Point,
  top?: string,
  bottom?: string,
  left?: string,
  right?: string
}) => (
  <div
    ref={e => handleRef(e, props.stateSize)}
    style={{
      overflow: 'hidden',
      borderColor: 'pink',
      borderStyle: 'solid',
      width: '20%',
      position: 'absolute',
      top: props.top,
      bottom: props.bottom,
      left: props.left,
      right: props.right
    }}
  >
    <img
      src={props.imageUrl}
      style={{
        transform: `translate(-${props.stateStart.x * 100}%,-${props.stateStart.y * 100}%)`,
        width: `${1 / props.stateSize.x * 100}%`,
        height: `${1 / props.stateSize.y * 100}%`
      }}
    />
  </div>
);

const handleRef = (e: HTMLDivElement | null, stateSize: Point) => {
  if (e) {
    const image = document.getElementById('labelImage');
    if (image) {
      const selectSize = {
        x: image.getBoundingClientRect().width * stateSize.x,
        y: image.getBoundingClientRect().height * stateSize.y
      };
      const zoomWidth = e.getBoundingClientRect().width;
      const selectRatio = selectSize.y / selectSize.x;
      e.style.height = `${zoomWidth * selectRatio}px`;
    }
  }
};
