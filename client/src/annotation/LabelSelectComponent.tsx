import * as React from 'react';
import { Button, ButtonGroup, ControlLabel, DropdownButton, FormGroup, InputGroup, MenuItem } from 'react-bootstrap';
import { Option } from '../util/Types';

interface Props {
  title?: string;
  idAddon?: string;
  saveButton?: string;
  label: string;
  labels: string[];
  colors: string[];
}

interface Actions {
  onChangeLabel: (label: string) => void;
  onSave?: (label: string) => void;
}

export const LabelSelect = (props: Props & Actions) => {
  return (
    <FormGroup>
      {props.title
        ? <ControlLabel>{props.title}</ControlLabel>
        : null
      }
      <InputGroup>
        <InputGroup.Addon
          style={{
            background: props.colors[props.labels.indexOf(props.label)],
            color: 'white'
          }}
        >{Option.of(props.idAddon).get(() => '_')}
        </InputGroup.Addon>
        <ButtonGroup justified={true}>
          <DropdownButton
            id="label-selector"
            componentClass={InputGroup.Button}
            title={props.label}
            style={{width: '100%'}}
          >
            {props.labels.map(label => (
              <MenuItem
                key={label}
                onClick={e => props.onChangeLabel(label)}
              >
                {label}
              </MenuItem>
            ))}
          </DropdownButton>
        </ButtonGroup>
        {Option
          .of(props.onSave)
          .map(onSave => (
            <InputGroup.Button>
              <Button onClick={e => onSave(props.label)}>
                {Option.of(props.saveButton).get(() => '+')}
              </Button>
            </InputGroup.Button>
          ))
          .get(() => <div/>)
        }
      </InputGroup>
    </FormGroup>
  );
};
