import { GenericPoint } from '../api/Point';
import * as React from 'react';

export const Rect = (props: {
  start?: GenericPoint<string>;
  end?: GenericPoint<string>;
  size: GenericPoint<string>;
  content?: JSX.Element | string;
  color?: string;
  background?: string
}) => {
  let position = {};
  if (props.start) {
    position = {top: props.start.y, left: props.start.x};
  }
  if (props.end) {
    position = {bottom: props.end.y, right: props.end.x};
  }
  return (
    <div
      draggable={false}
      style={{
        ...position,
        position: 'absolute',
        background: props.background,
        borderStyle: 'solid',
        color: props.color,
        borderColor: props.color,
        width: props.size.x,
        height: props.size.y,
      }}
    >
      {props.content}
    </div>
  );
};
