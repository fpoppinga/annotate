import { Point } from '../api/Point';
import { Box } from '../api/Image';
import { Option } from '../util/Types';
import { LabelsV2 } from '../api/LabelsV2';

export enum CurrentMove {
  P1, P2, NONE
}

export const ObjectColors = [
  '#AA0000',
  '#00AAAA',
  '#AA00AA',
  '#0000AA',
  '#000000',
  '#AAAA00',
  '#AAAAAA',
  '#00AA00'
];

export const ImageColors = [
  '#EEEEEE',
  '#AA0000',
  '#00AAAA',
  '#AAAA00',
  '#AA00AA',
  '#00AA00',
  '#0000AA',
  '#000000',
];

export interface State {
  initial: boolean;
  offset: Point;
  p1: Point;
  p2: Point;
  currentMove: CurrentMove;
  arrowUsed: boolean;
  label: {
    box: string;
    image: string;
  };
  labels: LabelsV2;
  image: string;
  box: Box[];
}

export interface OwnProps {
  authToken: Option<string>;
}

export interface Actions {
  onMouseDown: (e: React.MouseEvent<{}>) => void;
  onMouseMove: (e: React.MouseEvent<{}>) => void;
  onMouseUp: (e: React.MouseEvent<{}>) => void;
  onTouchStart: (e: React.TouchEvent<{}>) => void;
  onTouchMove: (e: React.TouchEvent<{}>) => void;
  onTouchEnd: (e: React.TouchEvent<{}>) => void;
  onArrow: (a: { e: KeyboardEvent, d: Point }) => void;
  onChangeLabel: (l: { box?: string, image?: string }) => void;
  onChangeExistingLabel: (v: { id: number, label: string }) => void;
  onCreateBox: (box: Box) => void;
  onDeleteBox: (boxIndex: number) => void;
  onSave: () => void;
  onLoadImage: (v: {}) => void;
  onLoadLabels: (v: {}) => void;
  onReset: () => void;
  onResetSelect: () => void;
}

export interface Props extends OwnProps {
  state: State;
}
