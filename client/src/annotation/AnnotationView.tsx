import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Button, Col, ControlLabel, Jumbotron, Row } from 'react-bootstrap';
import { Actions, CurrentMove, ImageColors, ObjectColors, Props } from './AnnotationModel';
import { LabelSelect } from './LabelSelectComponent';
import { KeyBinding } from '../util/KeyBinding';
import { GenericPoint, Point, Points } from '../api/Point';
import { Box } from '../api/Image';
import './AnnotationView.css';
import { renderIf } from '../util/Reacts';
import { Rect } from './RectComponent';
import { Preview } from './PreviewComponent';
import { Lifecycle } from '../Lifecycle';
import { Range, Stream } from '../util/Types';

export const Component = (props: Props & Actions) => {
  const stateStart = Points.reduce(props.state.p1, props.state.p2, Math.min);
  const stateEnd = Points.reduce(props.state.p1, props.state.p2, Math.max);
  const stateSize = Points.minus(stateEnd, stateStart);
  let ctrlPointSize = {x: '0.2em', y: '0.2em'};
  return (
    <Row>
      <Lifecycle
        componentDidMount={() => {
          props.onLoadImage({});
          props.onLoadLabels({});
        }}
      />
      <KeyBinding code="ArrowUp" onKey={e => props.onArrow({e: e, d: {x: 0, y: -.001}})}/>
      <KeyBinding code="ArrowDown" onKey={e => props.onArrow({e: e, d: {x: 0, y: .001}})}/>
      <KeyBinding code="ArrowLeft" onKey={e => props.onArrow({e: e, d: {x: -.001, y: 0}})}/>
      <KeyBinding code="ArrowRight" onKey={e => props.onArrow({e: e, d: {x: .001, y: 0}})}/>
      <KeyBinding code="Escape" onKey={e => props.onResetSelect()}/>
      <KeyBinding code="Enter" shift={true} onKey={e => window.confirm('Are you sure?') ? props.onSave() : null}/>
      <KeyBinding
        code="Enter"
        onKey={e => {
          props.onCreateBox(makeBox(props.state.label.box, props.state.p1, props.state.p2));
          props.onResetSelect();
        }}
      />
      {Stream.of(Range(1, Math.min(9, props.state.labels.boxes.length + 1)))
        .map((i: number) => (
          <KeyBinding
            key={i}
            code={`Digit${i}`}
            onKey={e => props.onChangeLabel({box: props.state.labels.boxes[i - 1]})}
          />
        )).toArray()}
      {props.state.labels.images
        .map(key => (
               <KeyBinding
                 key={key}
                 code={`Key${key[0].toUpperCase()}`}
                 onKey={e => props.onChangeLabel({image: key})}
               />
             )
        )
      }
      <Col xs={12} md={8}>
        <Jumbotron
          style={{background: ImageColors[props.state.labels.images.indexOf(props.state.label.image)]}}
          ref={e => preventTouchDefaults(e)}
          onMouseDown={e => props.onMouseDown(e)}
          onMouseMove={e => props.onMouseMove(e)}
          onMouseUp={e => props.onMouseUp(e)}
          onTouchStart={e => props.onTouchStart(e)}
          onTouchMove={e => props.onTouchMove(e)}
          onTouchEnd={e => props.onTouchEnd(e)}
        >
          <div
            id="labelImage"
            className="image draw-panel no-drag"
            draggable={false}
          >
            <img
              src={props.state.image}
              width="100%"
              draggable={false}
              className="no-drag"
            />
            {props.state.box.map((b, i) => (
              <Rect
                key={i}
                start={ToPercent(b.start)}
                size={ToPercent(b.size)}
                content={`${i}: ${b.label}`}
                color={ObjectColors[props.state.labels.boxes.indexOf(b.label)]}
              />
            ))}
            {renderIf(!props.state.initial, () => (
              <div>
                <Rect
                  start={ToPercent(stateStart)}
                  size={ToPercent(stateSize)}
                  content={props.state.label.box}
                  color="pink"
                />
                <Rect
                  start={ToPercent(props.state.p1)}
                  size={ctrlPointSize}
                  background="pink"
                  color="pink"
                />
                <Rect
                  start={ToPercent(props.state.p2)}
                  size={ctrlPointSize}
                  background="pink"
                  color="pink"
                />
              </div>
            ))}
            {renderIf((props.state.currentMove !== CurrentMove.NONE || props.state.arrowUsed), () => (
              <Preview
                imageUrl={props.state.image}
                stateStart={stateStart}
                stateSize={stateSize}
                left={stateStart.x > (1 - stateEnd.x) ? '0px' : undefined}
                right={stateStart.x <= (1 - stateEnd.x) ? '0px' : undefined}
                top={stateStart.y > (1 - stateEnd.y) ? '0px' : undefined}
                bottom={stateStart.y <= (1 - stateEnd.y) ? '0px' : undefined}
              />
            ))}
          </div>
        </Jumbotron>
      </Col>
      <Col xs={12} md={4}>
        <Jumbotron>
          <LabelSelect
            title="Image Label:"
            colors={ImageColors}
            label={props.state.label.image}
            labels={props.state.labels.images}
            onChangeLabel={l => props.onChangeLabel({image: l})}
          />
          <LabelSelect
            title="Add Label:"
            colors={ObjectColors}
            labels={props.state.labels.boxes}
            label={props.state.label.box}
            onChangeLabel={l => props.onChangeLabel({box: l})}
            onSave={l => {
              props.onCreateBox(makeBox(l, props.state.p1, props.state.p2));
              props.onResetSelect();
            }}
          />
          {
            props.state.box.length > 0 ? (
              <form>
                <ControlLabel>
                  Existing Labels:
                </ControlLabel>
                {props.state.box.map((box, i) => {
                  return (
                    <LabelSelect
                      key={i}
                      idAddon={`${i}`}
                      saveButton={'x'}
                      label={box.label}
                      labels={props.state.labels.boxes}
                      colors={ObjectColors}
                      onChangeLabel={label => props.onChangeExistingLabel({id: i, label: label})}
                      onSave={() => props.onDeleteBox(i)}
                    />
                  );
                })}
              </form>
            ) : null
          }
          <Button
            default={false}
            onClick={() => window.confirm('Are you sure?') ? props.onSave() : null}
          >
            {renderIf(
              props.state.box.length === 0,
              () => 'Send without Labels',
              () => 'Send Labels')}
          </Button>
        </Jumbotron>
      </Col>
    </Row>
  );
};

function makeBox(label: string, p1: Point, p2: Point): Box {
  const start = Points.reduce(p1, p2, Math.min);
  const size = Points.apply(Points.minus(p2, p1), Math.abs);
  return {
    label: label, start: start, size: size,
  };
}

const ToPercent = (p: Point): GenericPoint<string> => Points.apply(p, v => `${v * 100}%`);

const preventTouchDefaults = (el: Jumbotron | null) => {
  if (el) {
    ReactDOM.findDOMNode(el).addEventListener('touchstart', e => e.preventDefault());
    ReactDOM.findDOMNode(el).addEventListener('touchmove', e => e.preventDefault());
    ReactDOM.findDOMNode(el).addEventListener('touchend', e => e.preventDefault());
  }
};
