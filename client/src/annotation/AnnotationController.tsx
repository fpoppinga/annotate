import { Actions, CurrentMove, OwnProps, Props, State } from './AnnotationModel';
import { Action, ReducerFactory } from '../util/Reducers';
import * as Reacts from '../util/Reacts';
import { Point, Points } from '../api/Point';
import { connect, Dispatch } from 'react-redux';
import { RootState } from '../index';
import { Component } from './AnnotationView';
import { Box, Label, PostImage } from '../api/Image';
import { LabelsV2 } from '../api/LabelsV2';
import axios from 'axios';
import { Option } from '../util/Types';

const reducer = new ReducerFactory<State, Props>();

const ChangeLabel = reducer.createAction<{ box?: string, image?: string }>((s, a) => ({
  ...s,
  label: {
    box: Option.of(a.value.box).get(() => s.label.box),
    image: Option.of(a.value.image).get(() => s.label.image)
  }
}));

const ChangeExistingLabel = reducer.createAction<{ id: number, label: string }>((s, a) => ({
  ...s,
  box: s.box.map((b, i): Box => {
    if (i === a.value.id) {
      return {
        ...b,
        label: a.value.label
      };
    }
    return b;
  })
}));

const MouseDown = reducer.createAction<React.MouseEvent<{}>>((s, a) => {
  const p = Reacts.fromMouseEvent(Reacts.fromMouseClick(a.value), a.value);
  return HandleStart(s, p);
});

const TouchStart = reducer.createAction<React.TouchEvent<{}>>((s, a) => {
  a.value.preventDefault();
  const p = Reacts.fromMouseEvent(Reacts.fromTouchClick(a.value), a.value);
  return HandleStart(s, p);
});

const HandleStart = (s: State, p: Point) => {
  if (s.initial) {
    return {
      ...s,
      initial: false,
      currentMove: CurrentMove.P2,
      p1: p,
      p2: p
    };
  }
  const d1 = Points.squareAbs(Points.minus(s.p1, p));
  const d2 = Points.squareAbs(Points.minus(s.p2, p));
  if (d2 < d1) {
    return {
      ...s, currentMove: CurrentMove.P2, p2: p
    };
  } else {
    return {
      ...s, currentMove: CurrentMove.P1, p1: p
    };
  }
};

const MouseMove = reducer.createAction<React.MouseEvent<{}>>((s, a) => {
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  const p = Reacts.fromMouseEvent(Reacts.fromMouseClick(a.value), a.value);
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, p1: p
    };
  }
  return {
    ...s, p2: p
  };
});

const TouchMove = reducer.createAction<React.TouchEvent<{}>>((s, a) => {
  a.value.preventDefault();
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  const p = Reacts.fromMouseEvent(Reacts.fromTouchClick(a.value), a.value);
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, p1: p
    };
  }
  return {
    ...s, p2: p
  };
});

const ArrowMove = reducer.createAction<{ e: KeyboardEvent, d: Point }>((s, a) => {
  let factor = a.value.e.shiftKey ? 10 : 1;
  const diff = Points.times(a.value.d, factor);
  const min = {x: 0, y: 0};
  const max = {x: 1, y: 1};
  if (a.value.e.ctrlKey) {
    return {
      ...s,
      p1: Points.cap(Points.plus(s.p1, diff), min, max),
      initial: false,
      arrowUsed: true
    };
  } else {
    return {
      ...s,
      p2: Points.cap(Points.plus(s.p2, diff), min, max),
      initial: false,
      arrowUsed: true
    };
  }
});

const MouseUp = reducer.createAction<React.MouseEvent<{}>>((s, a) => {
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  const p = Reacts.fromMouseEvent(Reacts.fromMouseClick(a.value), a.value);
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, currentMove: CurrentMove.NONE, p1: p
    };
  } else {
    return {
      ...s, currentMove: CurrentMove.NONE, p2: p
    };
  }
});

const TouchEnd = reducer.createAction<React.TouchEvent<{}>>((s, a) => {
  a.value.preventDefault();
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, currentMove: CurrentMove.NONE
    };
  } else {
    return {
      ...s, currentMove: CurrentMove.NONE
    };
  }
});

const CreateBox = reducer.createAction<Box>(
  (state, box) => {
    if (!state.initial) {
      return {
        ...state,
        box: [...state.box, box.value]
      };
    }
    return state;
  }
);

const DeleteBox = reducer.createAction<number>(
  (state, boxIndex) => ({
    ...state,
    box: state.box.filter((b, i) => i !== boxIndex.value)
  })
);

const SaveImage = reducer.createVoidAction(
  (state, action) => {
    const auth = action.props.authToken
      .map(a => ({
        authToken: a
      } as {}))
      .get(() => ({}));
    const data: PostImage = {
      ...auth,
      label: {
        url: state.image,
        label: state.label.image,
        boxes: state.box.map(b => ({
          ...b,
          start: {
            x: b.start.x,
            y: b.start.y
          },
          size: {
            x: b.size.x,
            y: b.size.y
          }
        }))
      }
    };
    axios.post('/v2/image', data).then(() => {
      Reset.create(action.dispatch, action.props)();
      LoadImage.create(action.dispatch, action.props)();
      LoadLabels.create(action.dispatch, action.props)();
    });
    return state;
  }
);

const LoadLabels = reducer.createVoidAction(
  (state, action) => {
    axios
      .get('/v2/labels')
      .then(response => response.data as LabelsV2)
      .then(labels => UpdateLabels.create(action.dispatch, action.props)(labels));
    return state;
  });

const LoadImage = reducer.createVoidAction(
  (state, action) => {
    axios
      .get('/v2/image')
      .then(response => response.data as Label)
      .then(label => UpdateImage.create(action.dispatch, action.props)(label));
    return state;
  }
);

const UpdateImage = reducer.createAction<Label>(
  (state, action) => {
    return {
      ...state,
      image: action.value.url,
      label: {
        ...state.label,
        ...Option.of(action.value.label)
          .map<{}>(l => ({image: l}))
          .get(() => ({}))
      },
      box: action.value.boxes.map(b => ({
        ...b,
        start: {
          x: b.start.x,
          y: b.start.y
        },
        size: {
          x: b.size.x,
          y: b.size.y
        }
      }))
    };
  }
);

const UpdateLabels = reducer.createAction<LabelsV2>(
  (state, action) => ({
    ...state,
    label: {
      box: state.label.box === '' ? action.value.boxes[0] : state.label.box,
      image: state.label.image === '' ? action.value.images[0] : state.label.image
    },
    labels: action.value
  })
);

const Reset = reducer.createVoidAction((s) => initialState);

const ResetSelect = reducer.createVoidAction(s => ({
  ...s,
  ...initialSelectState
}));

const initialSelectState = {
  initial: true,
  arrowUsed: false,
  offset: {x: 0, y: 0},
  p1: {x: 0, y: 0},
  p2: {x: 1, y: 1},
  currentMove: CurrentMove.NONE,
};

const initialState: State = {
  ...initialSelectState,
  label: {
    box: '',
    image: ''
  },
  labels: {
    images: [],
    boxes: [],
  },
  box: [],
  image: ''
};

export const Reducer = reducer.build(initialState);

export const AnnotateImage = connect(
  (state: RootState, ownProps: OwnProps) => {
    return ({
      ...ownProps,
      state: state.annotation
    });
  },
  (dispatch: Dispatch<Action<{}, {}>>, props: Props): Actions => ({
    onMouseDown: MouseDown.create(dispatch, props),
    onMouseMove: MouseMove.create(dispatch, props),
    onMouseUp: MouseUp.create(dispatch, props),
    onTouchStart: TouchStart.create(dispatch, props),
    onTouchMove: TouchMove.create(dispatch, props),
    onTouchEnd: TouchEnd.create(dispatch, props),
    onArrow: ArrowMove.create(dispatch, props),
    onChangeLabel: ChangeLabel.create(dispatch, props),
    onCreateBox: CreateBox.create(dispatch, props),
    onDeleteBox: DeleteBox.create(dispatch, props),
    onSave: SaveImage.create(dispatch, props),
    onLoadImage: LoadImage.create(dispatch, props),
    onLoadLabels: LoadLabels.create(dispatch, props),
    onReset: Reset.create(dispatch, props),
    onResetSelect: ResetSelect.create(dispatch, props),
    onChangeExistingLabel: ChangeExistingLabel.create(dispatch, props)

  }))(Component);
