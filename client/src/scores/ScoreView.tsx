import { Actions, Props } from './ScoreModel';
import { Button, Jumbotron, Table } from 'react-bootstrap';
import * as React from 'react';
import { Lifecycle } from '../Lifecycle';
import { Stream } from '../util/Types';

export const Component = (props: Props & Actions) => (
  <Jumbotron>
    <Lifecycle
      componentDidMount={props.onLoad}
    />
    <h3>
      Highscores
    </h3>
    <Table>
      <thead>
      <tr>
        <th>Name</th>
        <th>Score</th>
      </tr>
      </thead>
      <tbody>
      {Stream
        .ofMap(props.state.scores)
        .toArray()
        .sort((a, b) => b.value - a.value)
        .map(v => (
          <tr key={v.key}>
            <td>{v.key}</td>
            <td>{v.value}</td>
          </tr>
        ))
      }
      </tbody>
    </Table>
    <Button
      onClick={e => props.onLoad()}
    >
      Refresh
    </Button>
  </Jumbotron>
);
