import { ReducerFactory } from '../util/Reducers';
import { Actions, OwnProps, Props, State } from './ScoreModel';
import { connect, Dispatch } from 'react-redux';
import { RootState } from '../index';
import { Scores } from '../api/Scores';
import axios from 'axios';
import { Component } from './ScoreView';

const actions = new ReducerFactory<State, OwnProps>();

const RequestScores = actions.createVoidAction((state, action) => {
  axios.get('/v1/scores')
    .then(r => r.data as Scores)
    .then(s => HandleScores.create(action.dispatch, action.props)(s));
  return state;
});

const HandleScores = actions.createAction<Scores>((s, a) => {
  return {
    ...s,
    scores: a.value
  };
});

const initialState: State = {
  scores: {}
};
export const Reducer = actions.build(initialState);

export const Score = connect(
  (rootState: RootState, ownProps: OwnProps): Props => ({
    state: rootState.scores
  }),
  (dispatch: Dispatch<{}>, ownProps: OwnProps): Actions => ({
    onLoad: RequestScores.create(dispatch, ownProps)
  })
)(Component);
