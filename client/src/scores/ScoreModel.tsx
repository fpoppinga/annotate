import { Scores } from '../api/Scores';

export interface OwnProps {

}

export interface State {
  scores: Scores
}

export interface Actions {
  onLoad: () => void
}

export interface Props extends OwnProps {
  state: State
}
