import { Option } from './util/Types';

export interface OwnProps {
  authToken: Option<string>;
}

export interface State {
}

export interface Actions {
}

export interface Props extends OwnProps {
  state: State;
}
