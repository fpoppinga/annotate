import { ReducerFactory } from './util/Reducers';
import { connect, Dispatch } from 'react-redux';
import { RootState } from './index';
import { Actions, OwnProps, Props, State } from './AppModel';
import { Component } from './AppView';

const actions = new ReducerFactory<State, OwnProps>();

const initialState: State = {};

export const AppReducer = actions.build(initialState);

export const App = connect(
  (rootState: RootState, ownProps: OwnProps): Props => {
    return {
      ...ownProps,
      authToken: rootState.login.login.map(l => l.accessToken),
      state: {
        ...rootState.app
      },
    };
  },
  (dispatch: Dispatch<{}>, props: OwnProps): Actions => {
    return {};
  }
)(Component);
