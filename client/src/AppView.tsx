import * as React from 'react';
import { AnnotateImage } from './annotation/AnnotationController';
import { Col, Grid, Navbar, Row } from 'react-bootstrap';
import { Actions, Props } from './AppModel';
import { Score } from './scores/ScoreController';
import { Readme } from './readme/ReadmeView';
import { LoginButton } from './login/LoginController';

export const Component = (props: Props & Actions) => {
  return (
    <div>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            annotate!
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Navbar.Form pullRight={true}>
            <LoginButton/>
          </Navbar.Form>
        </Navbar.Collapse>
      </Navbar>
      <Grid>
        <AnnotateImage
          authToken={props.authToken}
        />
        <Row>
          <Col xs={12} md={12}>
            <Score/>
          </Col>
        </Row>
        <Readme/>
      </Grid>
    </div>
  );
};
