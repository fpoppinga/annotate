import * as React from 'react';
import { Col, Jumbotron, Row, Table } from 'react-bootstrap';

export const Readme = () => (
  <Row>
    <Col xs={12} md={12}>
      <Jumbotron>
        <h3>Readme</h3>
        <h4>
          Keybindings
        </h4>
        <Table>
          <thead>
          <tr>
            <th>Key</th>
            <th>Effect</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>[Shift+]Arrows</td>
            <td>Move second control point. (Use Shift for fast mode)</td>
          </tr>
          <tr>
            <td>Ctrl+[Shift+]Arrows</td>
            <td>Move first control point. (Use Shift for fast mode)</td>
          </tr>
          <tr>
            <td>Numbers</td>
            <td>Select the label type</td>
          </tr>
          <tr>
            <td>Letters</td>
            <td>Select image label, according to first letter. (i.e., o → other)</td>
          </tr>
          <tr>
            <td>Enter</td>
            <td>Add current selection to labels</td>
          </tr>
          <tr>
            <td>Shift+Enter</td>
            <td>Send image to server</td>
          </tr>
          </tbody>
        </Table>
        <h4>How to label</h4>
        <ul>
          <li>Select the bounding boxes around objects that you can clearly identify.</li>
          <li>Surround the objects as close as possible, but completely.</li>
          <li>The robots own belly isn't a robot.</li>
          <li>An Image label annotates the whole situation on the image.</li>
          <ul>
            <li>Images, where the robot stands on the field during a game, should be labeled as 'game'</li>
            <li>Every other situation should be labeled as 'other'</li>
          </ul>
        </ul>
      </Jumbotron>
    </Col>
  </Row>
);
