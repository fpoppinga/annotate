import { connect, Dispatch } from 'react-redux';
import { Component } from './LoginView';
import { RootState } from '../index';
import { ReducerFactory } from '../util/Reducers';
import { Actions, OwnProps, Props, State } from './LoginModel';
import axios from 'axios';
import { LoginPostRequest, LoginPostResponse } from '../api/Login';
import { Option } from '../util/Types';

const actions = new ReducerFactory<State, Props>();

const initialState: State = {
  code: '',
  login: Option.of(localStorage.getItem('login'))
    .map(s => JSON.parse(s) as LoginPostResponse)
};

const RequestAccessToken = actions.createAction<string>((s, a) => {
  const request: LoginPostRequest = {
    code: a.value
  };
  axios
    .post('/v1/auth', request)
    .then(r => r.data as LoginPostResponse)
    .then(r => HandleLoginResponse.create(a.dispatch, a.props)(r));
  return s;
});

const HandleLoginResponse = actions.createAction<LoginPostResponse>((s, a) => {
  localStorage.setItem('login', JSON.stringify(a.value));
  return {
    ...s,
    login: Option.of(a.value)
  };
});

const Reset = actions.createVoidAction((s, a) => {
  localStorage.removeItem('login');
  return initialState;
});

export const Reducer = actions.build(initialState);

export const LoginButton = connect(
  (state: RootState, ownProps: OwnProps): Props => ({
    ...ownProps,
    state: state.login
  }),
  (dispatch: Dispatch<{}>, props: Props): Actions => ({
    onCode: RequestAccessToken.create(dispatch, props),
    onSignOut: Reset.create(dispatch, props)
  })
)
(Component);
