import { LoginPostResponse } from '../api/Login';
import { Option } from '../util/Types';

export interface OwnProps {

}

export interface State {
  code: string;
  login: Option<LoginPostResponse>;
}

export interface Actions {
  onCode: (code: String) => void;
  onSignOut: () => void;
}

export interface Props extends OwnProps {
  state: State;
}
