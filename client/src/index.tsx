import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';
import { combineReducers, createStore } from 'redux';
import { Reducer as AnnotationReducer } from './annotation/AnnotationController';
import { State as AnnotationState } from './annotation/AnnotationModel';
import { Provider } from 'react-redux';
import { State as AppState } from './AppModel';
import { State as LoginState } from './login/LoginModel';
import { Reducer as LoginReducer } from './login/LoginController';
import { App, AppReducer } from './AppController';
import { State as ScoreState } from './scores/ScoreModel';
import { Reducer as ScoreReducer } from './scores/ScoreController';

export interface RootState {
  annotation: AnnotationState;
  app: AppState;
  login: LoginState;
  scores: ScoreState;
}

const store = createStore(
  combineReducers(
    {
      app: AppReducer,
      annotation: AnnotationReducer,
      login: LoginReducer,
      scores: ScoreReducer
    })
);

ReactDOM.render(
  <Provider store={store}>
    <App
      authToken={(store.getState() as RootState).login.login.map(l => l.accessToken)}
    />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
